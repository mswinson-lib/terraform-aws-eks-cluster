# terraform-aws-eks-cluster

[Terraform Module]() for AWS EKS

## Features


## Usage

```HCL
module "demo-cluster" {
  source  = "bitbucket.org/mswinson-lib/terraform-aws-eks-cluster"

  cluster_name = "eks-cluster"  # default
  vpc_id = "<vpc id>"           # required
  subnet_ids = [<subnet_ids>]   # required
  instance_type = "m4.large"    # default
  max_size = 2                  # default
  min_size = 1                  # default
}
```

## Inputs

| Name | Description | Type | Default | Required |
| - | - | - | - | - |
| cluster_name | cluster name | string | eks-cluster | true |
| vpc_id | vpc id | string | | true |
| subnet_ids || list | | true |
| instance_type | instance type | string | m4.large | true |
| max_size || integer | 2 | true |
| min_size || integer | 1 | true |


## Outputs

None
