variable "organization" {}
variable "project" {}

variable "cluster_name" {
  type = "string"
  default = "eks-cluster"
}

variable "vpc_id" {}
variable "subnet_ids" {
  type = "list"
}

variable "instance_type" {
  type = "string"
  default = "m4.large"
}


data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

variable "max_size" {
  default = 2
}

variable "min_size" {
  default = 1
}

